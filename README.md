## ELK Stack
Filestructuur:
- **/log/** Directory waar te indexeren logfiles in dienen te worden geplaatst
- **/patterns/** Directory waarin custom pattern files kunnen worden geplaatst
- **/filebeat.yml** Configuratiefile voor Filebeat. Hierin staat welke type files moet worden geindexeerd en waar ze naartoe moeten worden gestuurd (Logstash). Let op! Deze file moet zijn aangemaakt door de root user (CHOWN root filebeat.yml) en mag alleen schrijfrechten hebben voor deze root user (CHMOD 644 filebeat.yml) 
- **/logstash.conf** Configuratiefile voor Logstash. Hierin staat waar de logfiles vandaan komen, worden filters in geconfigureerd en staat waar de geparste/verrijkte logfiles naartoe moeten worden gestuurd (ElasticSearch)

Bij docker-compose up worden 4 containers aangemaakt:
- **Filebeat** Logfiles worden geindexeerd en in beats doorgestuurd naar logstash
- **Logstash** Beats van verschillende bronnen worden gebundelt, gefilterd/verrijkt (zie logstash.conf) met metadata en doorgestuurd naar ElasticSearch
- **ElasticSearch** Search engine
- **Kibana** Dashboard (localhost:5501)